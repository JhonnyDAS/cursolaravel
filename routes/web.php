<?php

Route::get('/', function(){
    return view('welcome');
});

Route::resource('libro', 'LibroController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
