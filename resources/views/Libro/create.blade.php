<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Biblioteca</title>
</head>

<body>
    <div class="container">
        <h3 align="center">Nuevo Libro</h3>
        <form action="{{ route('libro.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="">Título</label>
                <input type="text" name="titulo" id="titulo" class="form-control @error('titulo') is-invalid @enderror"
                    value="{{ old('titulo') }}">
                @error('titulo')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="">Nro. Paginas</label>
                <input type="text" name="NroPaginas" id="NroPaginas"
                    class="form-control @error('titulo') is-invalid @enderror" value="{{ old('NroPaginas') }}">
                @error('NroPaginas')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="">Editorial</label>
                <input type="text" name="Editorial" id="Editorial"
                    class="form-control @error('titulo') is-invalid @enderror" value="{{ old('Editorial') }}">
                @error('Editorial')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </form>
    </div>
</body>

</html>
