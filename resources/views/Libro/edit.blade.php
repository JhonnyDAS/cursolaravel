<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Biblioteca</title>
</head>

<body>
    <div class="container">
        <h3 align="center">Modificar Libro</h3>
        <form action="{{ route('libro.update', $item->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <div class="form-group">
                <label for="">Título</label>
                <input type="text" name="titulo" id="titulo" class="form-control" value="{{ $item->titulo }}">
            </div>

            <div class="form-group">
                <label for="">Nro. Paginas</label>
                <input type="text" name="NroPaginas" id="NroPaginas" class="form-control"
                    value="{{ $item->NroPaginas }}">
            </div>

            <div class="form-group">
                <label for="">Editorial</label>
                <input type="text" name="Editorial" id="Editorial" class="form-control" value="{{ $item->Editorial }}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </form>
    </div>
</body>

</html>
