@extends('layouts.app')

@section('content')
<h1>LISTA DE LIBROS</h1>
<div class="d-flex">
    <a href="{{ route('libro.create') }}" class="btn btn-primary ml-auto">(+) Nuevo Libro</a>
</div>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">Titulo</th>
            <th scope="col">Nro. Páginas</th>
            <th scope="col">Editorial</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($libros as $item)
        <tr>
            <td>{{ $item->titulo }}</td>
            <td>{{ $item->NroPaginas }}</td>
            <td>{{ $item->Editorial }}</td>
            <td>
                <form action="{{ route('libro.destroy', $item->id) }}" method="post">
                    @csrf
                    {{ method_field('DELETE') }}
                    <a class="btn btn-primary" href="{{ route('libro.edit', $item->id) }}"><i
                            class="fas fa-edit"></i></a>
                    <button class="btn btn-danger" type="submit"
                        onclick="return confirm('¿Está seguro de eliminar este regitro?')"><i
                            class="fas fa-trash"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
